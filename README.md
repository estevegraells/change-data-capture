#Introducción
Este repositorio pertenece a la entrada http://forcegraells.com/2019/01/30/introduccion-salesforce-change-data-capture/ donde se explica como utilizar Change Data Capture en Salesforce.

## Dev, Build and Test

1. Solo es requiere desplegar el código en una ORG o Scratch ORG.
1. Incluir el componente en una página con Lightning Builder.
1. Activar CDC en cualquier objeto.
1. Al ejecutar la página deberás informar del channel del objeto que has activado en el paso anterior (si no sabes cual es, consulta el artículo, ya que depende de si es un objeto Standard o Custom).

## Resources
- La lista de recursos y documentación está disponible en el artículo

## Issues
N/A

#Demo

- Suscripción, generación y captura de eventos

![alt text](./img/Demo_CDC_EmpAPI.gif "Animación de la secuencia de acciones y eventos recibidos")
