/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Controller para recibir eventos
 */

({
  // Comprobamos que se haya cargado correctamente empApi
  onInit: function (component, event, helper) {
    // Get the empApi component
    const empApi = component.find("empApi");
    console.log('Buscando empApi component');

    // Uncomment below line to enable debug logging (optional)
    empApi.setDebugFlag(true);

    // Register error listener and pass in the error handler function
    empApi.onError(
      $A.getCallback(error => {
        // Error can be any type of error (subscribe, unsubscribe...)
        console.error("EMP API error: ", error);
      })
    );
  },

  // Función de suscripción qu utiliza la primitiva de empApi 
  subscribe: function (component, event, helper) {

    const empApi = component.find('empApi');

    // Get the channel from the input box
    const channel = component.find('channel').get('v.value');
    console.log("Petición de suscripción al canal: ", channel);

    // Replay option to get new events
    const replayId = -1;

    //Callback de recepción de nuevo evento recibido, creo dinámicamente un layoutItem y lo inserto 
    var eventoRecibidoCallback = function (eventoRecibido) {
      console.log("Nuevo Evento recibido: ", JSON.stringify(eventoRecibido.data.payload.ChangeEventHeader.changeType));

      var operacion = JSON.stringify(eventoRecibido.data.payload.ChangeEventHeader.changeType);
      var entidad = JSON.stringify(eventoRecibido.data.payload.ChangeEventHeader.entityName);
      var mensaje = "Se ha producido un " + operacion + " sobre la entidad: " + entidad;

      $A.createComponents([
        ["lightning:layoutItem", { "flexibility": "auto", "padding": "vertical-small", "size": "8", "class": "slds-p-top_large" }],
        ["lightning:card", { "title": "Evento Recibido" }],
        ["aura:html", { "tag": "p", "body": mensaje, HTMLAttributes: { "class": "slds-p-horizontal_small" } }]
      ], function (components, status, stausMessageList) {

        if (status == "SUCCESS") {

          //Anidamos los 2 tags p en el body del card
          components[1].set("v.body", components[2]);

          //Anidamos el card dentro del layoutItem
          components[0].set("v.body", components[1]);

          //Insertamos el resultado en la variable
          var listadoEventosRecibidosBody = component.get("v.listaEventosRecibidos");
          listadoEventosRecibidosBody.push(components[0]);

          //Actualizamos el valor lo que producirá una actualización de la UI
          component.set("v.listaEventosRecibidos", listadoEventosRecibidosBody);
        }

      });

    }.bind(this);

    // Suscribirse al Channel propiamente y fijar callback para recibir eventos
    empApi.subscribe(channel, replayId, eventoRecibidoCallback).then(function (subscription) {

      console.log("Suscripción correcta al canal: " + channel);

      //Fijamos el valor al atributo para la UI y para unsubscribe posterior
      component.set('v.subscription', subscription);
    });

  },

  // Cancelación de la suscripción
  unsubscribe: function (component, event, helper) {

    const empApi = component.find("empApi");
    const subscription = component.get("v.subscription");

    empApi.unsubscribe(
      subscription,
      $A.getCallback(unsubscribed => {
        console.log("Cancelación correcta de la suscripción del canal:  " + unsubscribed.subscription);
        component.set("v.subscription", null);

      })
    );
  }
});
