/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Trigger para la suscripción a un Data Change Event 
 */ 
 
trigger CDCOrderItem on OrderItemChangeEvent (after insert) {    
    for (OrderItemChangeEvent event : Trigger.New) {

        // Obtenemos el header y accedemos a sus campos
        EventBus.ChangeEventHeader header = event.ChangeEventHeader;
        System.debug('-ege- Evento de Objeto: ' + header.entityName);
        System.debug('-ege- Operación: ' + header.changeType);

        // Accedemos a los campos propios del Order Item 
        System.debug('-ege- Cantidad del Order Item: ' + event.Quantity);
        System.debug('-ege- Precio del Order Item: ' + event.UnitPrice);
    }
}